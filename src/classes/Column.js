import { pick } from '../helpers';

export default class Column {
    constructor(columnComponent) {
        const properties = pick(columnComponent, [
            'show', 'label', 'dataType', 'hidden', 'formatter', 'cellClass', 'headerClass', 'boldCell',
        ]);

        for (const property in properties) {
            this[property] = columnComponent[property];
        }

        this.template = columnComponent.$scopedSlots.default;
    }
}
